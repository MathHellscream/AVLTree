//
// Created by whoami on 07/11/16.
//

#ifndef AVLTREE_CTREEAVL_H
#define AVLTREE_CTREEAVL_H

#include "CNode.h"

template <typename T>
class CTreeAVL {
    CNode<T>* m_root;
    CTreeAVL(){ m_root = nullptr;}
    bool Find(T x, CNode<T>** &p);
    bool Insert(T x);
};


#endif //AVLTREE_CTREEAVL_H
