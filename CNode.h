//
// Created by whoami on 07/11/16.
//

#ifndef AVLTREE_CNODE_H
#define AVLTREE_CNODE_H

template <typename T>
class CNode {
    T m_data;
    CNode<T>* m_nodes[2];
    CNode(){ m_nodes[0]=m_nodes[1]=nullptr;}
    CNode(T x){ m_data=x; m_nodes[0]=nullptr; m_nodes[1]=nullptr;}
};


#endif //AVLTREE_CNODE_H
